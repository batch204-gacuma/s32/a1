let http = require("http");


http.createServer(function (request,response){
	if(request.url == "/profile"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to your Profile');
	}else if(request.url == "/courses"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Here's is our courses available");
	}else if(request.url == "/addcourse"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Add course to our resources");
	}else if(request.url == "/updatecourse"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("update a course to our resources");
	}else if(request.url == "/archivecourse"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end("Archive courses to our resources");
	}else{
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to Booking System');
	}
}).listen(4000);

console.log("server running at localhost:4000");
